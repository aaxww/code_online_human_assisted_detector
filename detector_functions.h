#ifndef detector_functions_h
#define detector_functions_h

#include "detector_functions.h"
#include "detector_classes.h"

// save image
void fun_save_image(cv::Mat img, long int number);

// draw message
void fun_draw_message(clsParameters* prms, cv::Mat img, char* text, CvPoint location, CvScalar textColor);

// draw rectangle
void fun_draw_rectangle(clsParameters* prms, cv::Mat img, CvRect rec, CvScalar recColor);

// show frame
void fun_show_frame(clsParameters* prms, cv::Mat img, int contFrame, double FPS);

// show image patch
void fun_show_image_patch(clsParameters* prms, cv::Mat img, clsDetectionSet* detSet);

// draw detection rectangle
void fun_draw_detection(cv::Mat img, clsDetection* det, CvScalar recColor, int recThick, float font);

// draw detections
void fun_draw_detections(clsParameters* prms, cv::Mat img, clsDetectionSet* detSet, cv::Mat labels);

// image equalization
void fun_image_equalization(cv::Mat img);

// detection labels
cv::Mat fun_detection_labels(clsParameters* prms, clsDetectionSet* detSet);

// fern maps
cv::Mat fun_fern_maps(cv::Mat img, clsRFs* RFs);

// scanning window
void fun_scanning_window(cv::Mat fernMaps, clsClassifier* classifier, clsDetectionSet* detSet);

// object detection
void fun_detect(clsParameters* prms, clsClassifier* classifier, clsRFs* RFs, cv::Mat img, clsDetectionSet* detSet);

// update classifier using positive samples
void fun_update_positive_samples(cv::Mat img, CvRect box, clsClassifier* clfr, clsRFs* RFs, int numSamples, float shift);

// update classifier using negative samples
void fun_update_negative_samples(cv::Mat img, CvRect box, clsClassifier* clfr, clsRFs* RFs, int numSamples, float shift);

// update classifier
void fun_update_classifier(clsParameters* prms, clsClassifier* clfr, clsRFs* RFs, cv::Mat img, clsDetectionSet* detSet, cv::Mat labels);

// train classifier
void fun_train_classifier(clsParameters* prms, clsClassifier* clfr, clsRFs* RFs, cv::Mat img, CvRect box);

#endif
