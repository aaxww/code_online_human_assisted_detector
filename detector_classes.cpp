#include <string>
#include <iostream>
#include <fstream>
#include "detector_classes.h"
#include <opencv2/imgproc/imgproc.hpp>
#include <opencv2/highgui/highgui.hpp>
#include <opencv2/core/core.hpp>

using namespace std;

// clsParameters
// constructor
clsParameters::clsParameters(){
  // initialize default parameters
  fun_initialize();
};
// destructor
clsParameters::~clsParameters(){
};
// initialize default parameters
void clsParameters::fun_initialize(){
  // default values
  this->objWidth = 18;  // object width
  this->objHeight = 18;  // object height
  this->imgWidth = 640;  // image width
  this->imgHeight = 480;  // image height
  this->numFerns = 10;  // num. random ferns
  this->numFeatures = 8;  // num. features
  this->fernSize = 8;  // fern size
  this->threshold = 0.58;  // classifier threshold
  this->omega = 0.2;  // assistance threshold
  this->numWCs = 300;  // num. weak classifiers
  this->recThick = 4;  // rectangle thickness
  this->imgShift = 0.15;  // image shift
  this->minCellSize = 5;  // min. image cell size
  this->maxCellSize = 20;  // max. image cell size
  this->numTrnSamples = 200;  // num. training samples
  this->numNewSamples = 2;  // num. new (boostrapped) samples
  this->font = 2.0;  // text font
  this->patchSize = 50;  // patch size
  this->imgEqualization = 1;  // image equalization
  this->saveImages = 0;  // save images
  this->color = cvScalar(0,255,0);  // rectangle color

  // parameter files
  this->filePath = "../files/parameters.txt";

};
// get image width
int clsParameters::fun_get_image_width(){
  return this->imgWidth;
};
// get image height
int clsParameters::fun_get_image_height(){
  return this->imgHeight;
};
// get object width
int clsParameters::fun_get_object_width(){
  return this->objWidth;
};
// get object height
int clsParameters::fun_get_object_height(){
  return this->objHeight;
};
// get num. ferns
int clsParameters::fun_get_num_ferns(){
  return this->numFerns;
};
// get num. features
int clsParameters::fun_get_num_features(){
  return this->numFeatures;
};
// get fern size
int clsParameters::fun_get_fern_size(){
  return this->fernSize;
};
// get num. weak classifiers
int clsParameters::fun_get_num_WCs(){
  return this->numWCs;
};
// get threshold
float clsParameters::fun_get_threshold(){
  return this->threshold;
};
// get assistance threshold
float clsParameters::fun_get_assistance_threshold(){
  return this->omega;
};
// get image shift
float clsParameters::fun_get_image_shift(){
  return this->imgShift;
};
// get thickness
int clsParameters::fun_get_rectangle_thickness(){
  return this->recThick;
};
// get min. cell size
int clsParameters::fun_get_min_cell_size(){
  return this->minCellSize;
};
// get max. cell size
int clsParameters::fun_get_max_cell_size(){
  return this->maxCellSize;
};
// get color
CvScalar clsParameters::fun_get_color(){
  return this->color;
};
// get text font
float clsParameters::fun_get_font(){
  return this->font;
};
// get the number of samples
int clsParameters::fun_get_num_train_samples(){
  return this->numTrnSamples;
};
// get the number of new samples
int clsParameters::fun_get_num_new_samples(){
  return this->numNewSamples;
};
// get patch size
int clsParameters::fun_get_patch_size(){
  return this->patchSize;
};
// get image equalization
int clsParameters::fun_image_equalization(){
  return this->imgEqualization;
};
// get save images
int clsParameters::fun_save_images(){
  return this->saveImages;
};
// load parameters
void clsParameters::fun_load_parameters(){

  // variables
  char buffer[128];
  FILE *ptr = NULL;
  int fontThick = 3;

  // open file
  if ((ptr = fopen(this->filePath, "r"))!= NULL){

    // read parameters
    fgets(buffer, 128, ptr);
    fscanf(ptr, "%s", buffer);
    fscanf(ptr, "%s %d", buffer, &this->imgWidth);  // image width
    fscanf(ptr, "%s %d", buffer, &this->imgHeight);  // image height
    fscanf(ptr, "%s %d", buffer, &this->objWidth);  // object width
    fscanf(ptr, "%s %d", buffer, &this->objHeight);  // object height
    fscanf(ptr, "%s %d", buffer, &this->numWCs);  // num. weak classifier -ferns at particular locations-
    fscanf(ptr, "%s %d", buffer, &this->fernSize);  // spatial fern size
    fscanf(ptr, "%s %d", buffer, &this->numFerns);  // num. shared random ferns
    fscanf(ptr, "%s %d", buffer, &this->numFeatures);  // num. binary features per fern
    fscanf(ptr, "%s %f", buffer, &this->threshold);  // classifier threshold
    fscanf(ptr, "%s %f", buffer, &this->omega);  //  human assistance threshold
    fscanf(ptr, "%s %d", buffer, &this->minCellSize);  // min. cell size -pyramide image-
    fscanf(ptr, "%s %d", buffer, &this->maxCellSize);  // max. cell size -pyramide image-
    fscanf(ptr, "%s %f", buffer, &this->imgShift);  // image shift factor 
    fscanf(ptr, "%s %d", buffer, &this->numTrnSamples);  // num. training samples
    fscanf(ptr, "%s %d", buffer, &this->numNewSamples);  // num. bootstrapped samples
    fscanf(ptr, "%s %d", buffer, &this->patchSize);  // object patch size
    fscanf(ptr, "%s %d", buffer, &this->imgEqualization);  // image equalization
    fscanf(ptr, "%s %d", buffer, &this->saveImages);  // saveImages
    fscanf(ptr, "%s %d", buffer, &this->recThick);  // detection rectangle thickness
    fscanf(ptr, "%s %f", buffer, &this->font);  // text font size

    // set color
    this->color = cvScalar(0,255,0);

    // close file pointer
    fclose(ptr);

  }
    else {
      cout << "ERROR : not input parameter file" << endl;
      exit(0);
  }
};
// print parameters
void clsParameters::fun_print_parameters(){

  cout << "\n*********************************************************" << endl;
  cout << "*   Online Human-Assisted Learning using Random Ferns   *" << endl;
  cout << "*                    Michael Villamizar                 *" << endl;
  cout << "*                 mvillami.at.iri.upc.edu               *" << endl;
  cout << "*                          2015                         *" << endl;
  cout << "*********************************************************" << endl;

  cout << "\n*********************************************************" << endl;
  cout << "* Program parameters :" << endl;
  cout << "* image size -> " << this->imgHeight << "x" << this->imgWidth << endl;
  cout << "* object height -> " << this->objHeight << endl;
  cout << "* num. weak classifiers -> " << this->numWCs << endl;
  cout << "* num. random ferns -> " << this->numFerns << endl;
  cout << "* num. features -> " << this->numFeatures << endl;
  cout << "* fern size -> " << this->fernSize << "x" << this->fernSize << endl;
  cout << "* threshold -> " << this->threshold << endl;
  cout << "* omega -> " << this->omega << endl;
  cout << "* num. training samples -> " << this->numTrnSamples << endl;
  cout << "* num. new samples -> " << this->numNewSamples << endl;
  cout << "* min. cell size -> " << this->minCellSize << endl;
  cout << "* max. cell size -> " << this->maxCellSize << endl;
  cout << "* image shift -> " << this->imgShift << endl;
  cout << "* rec. thickness -> " << this->recThick<< endl;
  cout << "*********************************************************" << endl;
};




// clsRFs
// constructor
clsRFs::clsRFs(){
  // initialize default parameters
  this->numFerns = 5;  // num. random ferns
  this->fernSize = 4;  // fern size
  this->fernDepth = 3;  // fern depth
  this->numFeatures = 5;  // num. features per fern
};
// destructor
clsRFs::~clsRFs(){
};
// get number of random ferns
int clsRFs::fun_get_num_ferns(){
  return this->numFerns;
};
// get the number of features
int clsRFs::fun_get_num_features(){
  return this->numFeatures;
};
// get the fern size
int clsRFs::fun_get_fern_size(){
  return this->fernSize;
};
// get random ferns
cv::Mat clsRFs::fun_get_random_ferns(){
  return this->data;
};
// compute random ferns
void clsRFs::fun_random_ferns(int ferns, int features, int size){

  // set random ferns values
  this->numFerns = ferns;  // num. of random ferns
  this->numFeatures = features;  // num. features inside fern
  this->fernSize = size;  // fern size

  // allocate data
  this->data = cv::Mat(this->numFerns, this->numFeatures, CV_8UC(6));

  // pointer to ferns data
  unsigned char *fernsPtr = (unsigned char*)(data.data);

  // random ferns
  for (int fern = 0; fern<this->numFerns; fern++ ){
    for (int feature = 0; feature<this->numFeatures; feature++){

      // feature coordinates
      int ya = floor(this->fernSize*((double)rand()/RAND_MAX));
      int xa = floor(this->fernSize*((double)rand()/RAND_MAX));
      int ca = floor(this->fernDepth*((double)rand()/RAND_MAX));

      // feature coordinates
      int yb = floor(this->fernSize*((double)rand()/RAND_MAX));
      int xb = floor(this->fernSize*((double)rand()/RAND_MAX));
      int cb = floor(this->fernDepth*((double)rand()/RAND_MAX));

      // save
      *(fernsPtr + fern*this->numFeatures*6 + feature*6 + 0) = ya;
      *(fernsPtr + fern*this->numFeatures*6 + feature*6 + 1) = xa;
      *(fernsPtr + fern*this->numFeatures*6 + feature*6 + 2) = ca;
      *(fernsPtr + fern*this->numFeatures*6 + feature*6 + 3) = yb;
      *(fernsPtr + fern*this->numFeatures*6 + feature*6 + 4) = xb;
      *(fernsPtr + fern*this->numFeatures*6 + feature*6 + 5) = cb;
    }
  }
};
// print ferns values
void clsRFs::fun_print_ferns(){
  cout << "\n*********************************************************" << endl;
  cout << "* Random Fern Parameters : " << endl;
  cout << "* num. ferns -> " << this->numFerns << endl;
  cout << "* num. features -> " << this->numFeatures << endl;
  cout << "* fern size ->  " << this->fernSize << "x" << this->fernSize << endl;
  cout << "* fern depth ->  " << this->fernDepth << endl;
  cout << "*********************************************************\n" << endl;
};




// clsClassifier
// constructor
clsClassifier::clsClassifier(){
  // initialize default parameters
  this->numWCs = 100;  // number of weak classifiers
  this->objWidth = 24;  // object width
  this->objHeight = 24;  // object height
  this->threshold = 0.5;  // classifier threshold
};
// destructor
clsClassifier::~clsClassifier(){
};
// get threshold
float clsClassifier::fun_get_threshold(){
  return this->threshold;
};
// set threshold
void clsClassifier::fun_set_threshold(float thr){
  this->threshold = thr;
};
// get object size
void clsClassifier::fun_get_object_size(int& height, int& width){
  height = this->objHeight;
  width = this->objWidth;
};
// set object size
void clsClassifier::fun_set_object_size(int height, int width ){
  this->objHeight = height;
  this->objWidth = width;
};
// get num. weak classifiers
int clsClassifier::fun_get_num_WCs(){
  return this->numWCs;
};
// get weak classifiers
cv::Mat clsClassifier::fun_get_WCs(){
  return this->WCs;
};
// get positive fern distributions
cv::Mat clsClassifier::fun_get_posHstms(){
  return this->posHstms;
};
// get negative fern distributions
cv::Mat clsClassifier::fun_get_negHstms(){
  return this->negHstms;
};
// get ratio of fern distributions
cv::Mat clsClassifier::fun_get_ratHstms(){
  return this->ratHstms;
};
// compute the classifier
void clsClassifier::fun_compute(int numWCs, int numFerns, int numFeatures, int fernSize){

  // set the number of weak classifiers and histogram bins
  this->numWCs = numWCs;
  this->numBins = (int)pow(2, numFeatures);

  // weak classifiers
  this->WCs = cv::Mat(this->numWCs, 3 , CV_8UC1);

  // pointer to weak classifiers data
  unsigned char *WCsPtr = (unsigned char*)(this->WCs.data);

  // weak classifiers
  for (int WC = 0; WC<this->numWCs; WC++){

    // random weak classifier: fern at specific location
    int y = floor((this->objHeight-fernSize)*((float)rand()/RAND_MAX));
    int x = floor((this->objWidth-fernSize)*((float)rand()/RAND_MAX));
    int f = floor(numFerns*((float)rand()/RAND_MAX));

    // save
    *(WCsPtr + WC*3 + 0) = y;
    *(WCsPtr + WC*3 + 1) = x;
    *(WCsPtr + WC*3 + 2) = f;

  }

  // distributions
  this->posHstms = cv::Mat(this->numWCs, this->numBins, CV_32FC1, cv::Scalar::all(1.0));
  this->negHstms = cv::Mat(this->numWCs, this->numBins, CV_32FC1, cv::Scalar::all(1.0));
  this->ratHstms = cv::Mat(this->numWCs, this->numBins, CV_32FC1, cv::Scalar::all(0.5));

};
// update the classifier
void clsClassifier::fun_update(cv::Mat &fernsMaps, float label){

  // fern map size
  int width = fernsMaps.cols;
  int height = fernsMaps.rows;
  int numFerns = fernsMaps.channels();

  // pointer to weak classifiers, ferns maps, positive, negative and ratio fern distributions
  unsigned char *WCsPtr = (unsigned char*)(this->WCs.data);
  unsigned short *mapsPtr = (unsigned short*)(fernsMaps.data);
  float* posPtr = this->posHstms.ptr<float>(0);
  float* negPtr = this->negHstms.ptr<float>(0);
  float* ratPtr = this->ratHstms.ptr<float>(0);

  // weak classifiers
  for (int WC=0; WC<this->numWCs; WC++){

    // weak classifier data: fern location
    int y = (int)*(WCsPtr + WC*3 + 0);
    int x = (int)*(WCsPtr + WC*3 + 1);
    int f = (int)*(WCsPtr + WC*3 + 2);

    // fern output
    int z = (int)*(mapsPtr + y*width*numFerns + x*numFerns + f);

    // update distributions
    if (label==1.0){
      *(posPtr + WC*this->numBins + z)+= 1.0;
    }
    if (label==-1.0){
      *(negPtr + WC*this->numBins + z)+= 1.0;
    }

    // ratio fern distribution
    float pos = *(posPtr + WC*this->numBins + z);
    float neg = *(negPtr + WC*this->numBins + z);
    *(ratPtr + WC*this->numBins + z) = pos/(pos+neg);
  }
};
// print classifier parameters
void clsClassifier::fun_print_classifier(){
  cout << "\n*********************************************************" << endl;
  cout << "* Classifier Parameters : " << endl;
  cout << "* object size ->  " << this->objHeight << "x" << this->objWidth << endl;
  cout << "* num. weak classifiers -> " << this->numWCs << endl;
  cout << "* threshold -> " << this->threshold << endl;
  cout << "*********************************************************\n" << endl;
};





// clsDetection
// constructor
clsDetection::clsDetection(){
  // initialize default values
  fun_initialize();
};
// destructor
clsDetection::~clsDetection(){
};
// initialize with default values
void clsDetection::fun_initialize(){
  // default parameters
  this->x1 = 0;  // detection location
  this->y1 = 0;
  this->x2 = 0;
  this->y2 = 0;
  this->score = 0;  // detection score
};
// set values
void clsDetection::fun_set_values(int xa, int ya, int xb, int yb, float value) {
  this->x1 = xa;  // detection location
  this->y1 = ya;
  this->x2 = xb;
  this->y2 = yb;
  this->score = value;  // detection score
};
// get values
void clsDetection::fun_get_values (int &xa, int &ya, int &xb, int &yb, float &value) {
  xa = this->x1;  // detection coordinates
  ya = this->y1;
  xb = this->x2;
  yb = this->y2;
  value = this->score;  // detection score
};



// clsDetectionSet
// constructor
clsDetectionSet::clsDetectionSet(){
  // initialize
  fun_initialize();
};
// Destructor
clsDetectionSet::~clsDetectionSet(){
  // relase memory
  fun_release();
};
// initialize
void clsDetectionSet::fun_initialize(){

  // default values
  this->numDetections = 0;  // number of detections
  this->numMaxDetections = 1000;  // number muximum of detections

  // array of detections
  this->detection = new clsDetection[this->numMaxDetections];
};
// release
void clsDetectionSet::fun_release(){
  delete[]this->detection;
};
// get the number of detections
int clsDetectionSet::fun_get_num_detections(){
  return this->numDetections;
};
// set the number of detections
void clsDetectionSet::fun_set_num_detections(int value){
  this->numDetections = value;
};
// get the number muximum of detections
int clsDetectionSet::fun_get_num_max_detections(){
  return this->numMaxDetections;
};
// get detection
clsDetection* clsDetectionSet::fun_get_detection(int index){
  clsDetection* det = &this->detection[index];
  return det;
};
// set a new detection
void clsDetectionSet::fun_set_detection(clsDetection* det, int index){

  // detection variable
  float score;
  int x1,y1,x2,y2;

  // get input detection values
  det->fun_get_values(x1, y1, x2, y2, score);

  // set detection values
  this->detection[index].fun_set_values(x1,y1,x2,y2,score);
};
// scaling
void clsDetectionSet::fun_scaling(float scaleFactor){

  // detection variables
  float score;
  int y1,x1,y2,x2;

  // scaling detection coordinates
  for (int iter=0; iter<this->numDetections; iter++ ){

    // scaling spatial coordinates
    this->detection[iter].fun_get_values(x1, y1, x2, y2, score);
    x1 = (int)round(x1*scaleFactor);
    y1 = (int)round(y1*scaleFactor);
    x2 = (int)round(x2*scaleFactor);
    y2 = (int)round(y2*scaleFactor);
    this->detection[iter].fun_set_values(x1, y1, x2, y2, score);
  }
};
// add detections
void clsDetectionSet::fun_add_detections(clsDetectionSet* newDetections){

  // detections
  int numNewDets = newDetections->fun_get_num_detections();

  // check
  if (this->numDetections + numNewDets >= this->numMaxDetections){
    printf("!! ERROR: num. max. detections is %d \n",this->numMaxDetections);
  }
  else{
    // add detections
    for (int iter=0; iter<numNewDets; iter++){
      this->detection[numDetections+iter] = newDetections->detection[iter];
    }
    // updates the number of detections
    this->numDetections += numNewDets;
  }
};
// maxima detection
void clsDetectionSet::fun_get_max_detection(clsDetection* maxDet){

  // variables
  int x1,y1,x2,y2;
  float score,maxScore = 0;

  // detections
  for (int iter=0; iter<this->numDetections; iter++){

    // get detection values
    this->detection[iter].fun_get_values(x1, y1, x2, y2, score);

    // best score
    if (score>maxScore){

      // set the max. detection
      maxDet->fun_set_values(x1, y1, x2, y2, score);

      // update max. score
      maxScore = score;
    }
  }
};
// remove detections 
void clsDetectionSet::fun_remove_detections(clsDetection* det){

  // parameters
  float thr = 0.01;  // overlapping rate threshold

  // variables
  int counter = 0;  // counter
  int x1,y1,x2,y2;  // detection coordinates
  int rx1,ry1,rx2,ry2;  // reference detection coordinates
  clsDetection empty;  // empty detection
  float score,rscore;  // detection scores

  // number of initial detections
  this->numDetections = fun_get_num_detections();

  // get reference detection values
  det->fun_get_values(rx1, ry1, rx2, ry2, rscore);

  // reference area
  float rarea = (ry2-ry1)*(rx2-rx1);

  // check each detection
  for (int iter=0; iter<this->numDetections; iter++){

    // get detection values
    this->detection[iter].fun_get_values(x1, y1, x2, y2, score);

    // overlapping values
    int u1 = max(rx1,x1);
    int v1 = max(ry1,y1);
    int u2 = min(rx2,x2);
    int v2 = min(ry2,y2);
    float width  = u2-u1;
    float height = v2-v1;
    float area = width*height;

    // remove detections
    if (width<=0 || height<=0 || (area/rarea)<thr){
      // no intersection
      this->detection[counter].fun_set_values(x1, y1, x2, y2, score);
      counter++;
    }
  }

  // delete detections
  for (int iter=counter; iter<this->numDetections; iter++){
    this->detection[iter] = empty;
  }

  // update the number of detections
  fun_set_num_detections(counter);

};
// non-maxima suppresion
void clsDetectionSet::fun_non_maxima_supression(){

  // variables
  int counter = 0;   // detection counter

  // temporal detection set
  clsDetectionSet* detSet = new clsDetectionSet;

  // number of initial detections
  this->numDetections = fun_get_num_detections();

  // check detections
  if (this->numDetections>0){

    // remove detections
    while (this->fun_get_num_detections()!=0){

      // max. detection
      clsDetection* maxDet = new clsDetection;

      // get max. detection
      this->fun_get_max_detection(maxDet);

      // get non-intersection detections
      this->fun_remove_detections(maxDet);

      // add detection: max. detection
      detSet->fun_set_detection(maxDet, counter);

      // update counter
      counter++;

      // set the number of detections
      detSet->fun_set_num_detections(counter);

      // release
      delete maxDet;
    }
  }

  // add max. detections: non-maxima suppression
  this->fun_add_detections(detSet);

  // release
  delete detSet;
};



// clsII
// constructor
clsII::clsII(){
  // default values
  this->width = 0;  // integral image width
  this->height = 0;  // integral image height
  this->imgWidth = 0;  // image width
  this->imgHeight = 0;  // image height
  this->numChannels = 3;  // number of image feature channels
};
// destructor
clsII::~clsII(){
};
// get integral image
cv::Mat clsII::fun_get_image() {
  return this->img;
};
// get image size
void clsII::fun_get_image_size(int& sx, int& sy) {
  sx = this->imgWidth;
  sy = this->imgHeight;
};
// compute image
void clsII::fun_compute_image(int size){

  // set values
  this->cellSize = size;  // cell size
  this->imgWidth = (int)ceil((float)this->width/this->cellSize);  // image width
  this->imgHeight = (int)ceil((float)this->height/this->cellSize);  // image height

  // variables
  int w = this->width;
  int h = this->height;
  int n = this->numChannels;
  int s = this->cellSize;
  int t,b,l,r;
  double tl,bl,tr,br,value;

  // create image
  this->img = cv::Mat(cvSize(this->imgWidth,this->imgHeight), CV_8UC(this->numChannels), cv::Scalar::all(0));

  // pointer to image and integral image data
  double* IIPtr = this->II.ptr<double>(0);
  unsigned char *imgPtr = (unsigned char*)(this->img.data);

  // scanning
  for (int y=0; y<this->imgHeight-1; y++) {

    // local coordinates
    t = y*s;  // top
    b = t + s;  // bottom

    for (int x=0; x<this->imgWidth-1; x++) {

      // local coordinates
      l = x*s;  // left
      r = l + s;  // right

      // check
      if (t<0 || b>=h || l<0 || r>=w)
        printf("Warning: incorrect corner coordinates -> top: %d left: %d bottom: %d right: %d img. height %d img. width%d \n",t,l,b,r,h,w);

      for (int c=0; c<this->numChannels; c++){

        // integral image values
        tl = *(IIPtr + t*w*n + l*n + c);  // top-left corner
        bl = *(IIPtr + b*w*n + l*n + c);  // bottom-left corner
        tr = *(IIPtr + t*w*n + r*n + c);  // top-right corner
        br = *(IIPtr + b*w*n + r*n + c);  // bottom-right corner

        // check
        if (tl<0 || bl<0 || tr<0 || br<0)
          printf("Warning: incorrect corner values -> top-left: %.3f bottom-left: %.3f top-right: %.3f bottom:right: %.3f \n",tl,bl,tr,br);

        // image value
        value = br + tl - tr - bl;
        value = (double)value/(s*s);

        // check (2 -> small error)
        if (value<0 || value>255+2)
          printf("Warning: incorrect image value -> %.3f \n",value);

        // image
        *(img.data + y*this->imgWidth*n + x*n + c) = (int) value;

      }
    }
  }
};
// compute integral image
void clsII::fun_integral_image(cv::Mat image){

  // set II size
  this->width = image.cols;
  this->height = image.rows;
  this->numChannels = image.channels();

  // create II
  this->II = cv::Mat(this->height, this->width, CV_64FC(this->numChannels), cv::Scalar::all(0));

  // variables
  int w = this->width;
  int h = this->height;
  int n = this->numChannels;
  double imgValue,values;

  // pointers to image and integral image data
  double* IIPtr = II.ptr<double>(0);
  unsigned char *imgPtr = (unsigned char*)(image.data);

  // construction
  for (int y=0; y<this->height; y++){
    for (int x=0; x<this->width; x++){
      for (int c=0; c<this->numChannels; c++){

        // image pixel value
        imgValue = *(imgPtr + y*w*n + x*n + c);

        // integral image values
        if (x>0){
          if (y>0){
            values = *(IIPtr + y*w*n + (x-1)*n + c) + *(IIPtr + (y-1)*w*n + x*n + c) - *(IIPtr + (y-1)*w*n + (x-1)*n + c);
          }
          else {
            values = *(IIPtr + y*w*n + (x-1)*n + c);
          }
        }
        else {
          if (y>0){
            values = *(IIPtr + (y-1)*w*n + x*n + c);
          }
          else {
            values = 0;
          }
        }

        // compute current pixel value in II
        *(IIPtr + y*w*n + x*n + c) = values + imgValue;

        // check
        if (values + imgValue > (y+1)*(x+1)*255)
          printf("Warning incorrect integral image value -> %.2f\n",values + imgValue);

      }
    }
  }
};
// compute integral image
void clsII::fun_release_image(){
  // release image
  this->img.release();
};








