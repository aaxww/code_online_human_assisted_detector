/*

  Online Human-Assisted Learning using Random Ferns

  Description:
    This file contains the code to perform online and human-assisted learning
    using random ferns [1][2]. More specifically, this code allows to learn
    and detect simultaneously one specific object using human assistance.
    Initially, the human selects via the computer's mouse the object in the
    image that he/she wants to learn and recognize in future frames.
    Subsequently, the random ferns classifier is initially computed using
    a set of training samples. Positive samples are extracted using random
    shift transformations over the object, whereas negative samples are random
    patches from the background.

    In run time, the random ferns detect the object and use their own hypotheses
    to update and refine the classifier (self-learning). However, in cases where
    the classifier is uncertain about its output (sample label), the classifier
    requests the human assistance in order to label the difficult samples. In this
    case, the program asks to the human user if the object detection (bounding box)
    is correct or not. The user should answer yes (y) or not (n) via the keyboard.

    If you make use of this code for research articles, we kindly encourage
    to cite the references [1][2], listed below. This code is only for research
    and educational purposes.

  Requirements:
    1. opencv (e.g opencv 2.4.9)
    2. cmake

  Compilation:
    1. mkdir build
    2. cd build
    3. cmake ..
    3. make
    4. ./detector (using webcam)
 
  Comments:
    1. The program works at any input image resolution. However, the screen
       information like score, results, etc, are displayed for a resolution
       of 640x480 pixels. This is the default resolution. If you change the
       resolution you must sure that the functions: fun_show_frame and
       fun_update_classifiers are shown properly. We recommend not using a
       resolution lower of 640x480 pixels.
    2. The program parameters are defined in the file parameters.txt located
       in the files folder. In this file you can change the detector parameters
       and program funcionalities.

  Keyboard commands:
    p: pause the video stream
    ESC: exit the program
    space: enable/disable object detection
    y: yes
    n: no

  Contact:
    Michael Villamizar
    mvillami@iri.upc.edu
    Institut de Robòtica i Informàtica Industrial, CSIC-UPC
    Barcelona - Spain
    2015

  References:
    [1] Online Human-Assisted Learning using Random Ferns
        M. Villamizar, A. Garrell, A. Sanfeliu and F. Moreno-Noguer
        International Conference on Pattern Recognition (ICPR), 2012.

    [2] Proactive Behavior of an Autonomous Mobile Robot for Human-Assisted Learning
        A. Garrell, M. Villamizar, F. Moreno-Noguer and A. Sanfeliu
        International Symposium on Robot and Human Interactive Communication (IEEE RO-MAN), 2013.

*/

#include <list>
#include <time.h>
#include <stdio.h>
#include <string>
#include <iostream>
#include <signal.h>
#include "detector_classes.h"
#include "detector_functions.h"
#include <opencv2/imgproc/imgproc.hpp>
#include <opencv2/highgui/highgui.hpp>
#include <opencv2/core/core.hpp>

using namespace std;

// global variables
bool flg_exit = false;  // exit program
bool flg_train = false;  // enable training
bool flg_pause = false;  // pause the program
bool flg_drawing = false;  // enable drawing circle
bool flg_detection = false;  // enable detection
CvRect rec = cvRect(-1,-1,0,0);  // bounding box (target)

// functions
void fun_mouse_callback(int event, int x, int y, int flags, void* param);
void fun_keyboard_callback(clsParameters* prms, cv::Mat &img, char key);

// main function
int main( int argc, char* argv[] ){

  // variables
  char key;  // keyboard
  int numDets;  // num. detections
  CvScalar color;  // color
  cv::Mat frame;  // video frame
  time_t start,end;  // times
  clsDetection* det;  // detection
  int height,width;  // image heigth and width
  double dif,FPS = 0;  // diff. time and frames per second
  long int contFrame = 0;  // frame counter
  unsigned short int contFPS = 0;  // frames per second counter

  // program parameters
  clsParameters* prms = new clsParameters;  // initialize parameters
  prms->fun_load_parameters();  // load parameters from file
  prms->fun_print_parameters();  // print program parameters

  // random ferns
  clsRFs* RFs = new clsRFs;  // initialize random ferns
  RFs->fun_random_ferns(prms->fun_get_num_ferns(),prms->fun_get_num_features(),prms->fun_get_fern_size());  // create random ferns
  RFs->fun_print_ferns();  // print ferns parameters

  // object classifier
  clsClassifier* clfr = new clsClassifier;  // initialize the classifier

  // image size
  width = prms->fun_get_image_width();  // width
  height = prms->fun_get_image_height();  // height

  // default color
  color = prms->fun_get_color();

  // video capture
  cv::VideoCapture capture(0); // webcam

  // check if we succeeded
  if(!capture.isOpened()){ 
    cout << "Couldn't capture stream" << endl;
    return -1;
  }

  // window
  cv::namedWindow("detection", CV_WINDOW_AUTOSIZE);

  // initial clock
  time(&start);

  // stream
  while(1){

    // object detections
    clsDetectionSet* detSet = new clsDetectionSet;

    // video frame
    if (!flg_pause)
      capture >> frame; 

    // create and resize input image
    cv::Mat img = frame;
    cv::resize(img, img, cvSize(width,height));

    // image equalization
    if (prms->fun_image_equalization()==1)
      fun_image_equalization(img);

    // bounding box
    cv::setMouseCallback("detection", fun_mouse_callback, &img);

    // show bounding box
    if (flg_drawing){
      fun_draw_message(prms, img, "bounding box", cvPoint(10, height-30), color);
      fun_draw_rectangle(prms, img, rec, color);
    }

    // check stop
    if (!flg_pause){

      // train step
      if (flg_train){

        // set/reset variable
        flg_train = false;
        flg_detection = true;

        // train classifier
        fun_train_classifier(prms, clfr, RFs, img, rec);
      }

      // check detection
      if (flg_detection){

        // object detection
        fun_detect(prms, clfr, RFs, img, detSet);

        // num. detections
        numDets = detSet->fun_get_num_detections();

        // check
        if (numDets>0){

          // detection labels
          cv::Mat labels = fun_detection_labels(prms, detSet);

          // update classifier
          fun_update_classifier(prms, clfr, RFs, img, detSet, labels);

          // draw detections
          fun_draw_detections(prms, img, detSet, labels);
        }

        // max. detection score and image patch
        fun_show_image_patch(prms, img, detSet);

      }

      // update counters
      contFPS++;
      contFrame++;

      // times
      time(&end);
      dif = difftime(end,start);
      if (dif>=1.){
        FPS = contFPS/dif;
        printf("Frame count -> %ld : FPS -> %.4f \n",
        contFrame, FPS);
        contFPS = 0;
        time(&start);
      }

      // show frame message
      fun_show_frame(prms, img, contFrame, FPS);

      // save image
      if (prms->fun_save_images()==1)
        fun_save_image(img, contFrame);

    }

    // show image
    cv::imshow("detection", img);
    key = cv::waitKey(10);

    // keyboard commands
    fun_keyboard_callback(prms, img, key);

    // release
    delete detSet;

    // break point
    if (flg_exit) break;
  }

  // release
  delete prms,RFs,clfr;
  cv::destroyWindow("detection");
  return 0;
}

// mouse callback
void fun_mouse_callback(int event, int x, int y, int flags, void* param ){

  // input image
  cv::Mat* img = (cv::Mat*) param;

  // events
  switch (event){
    // mouse move
    case CV_EVENT_MOUSEMOVE:{
      if (flg_drawing){
        rec.width  = x - rec.x;
        rec.height = y - rec.y;
      }
    }
    break;

    // left button down
    case CV_EVENT_LBUTTONDOWN:{
      flg_drawing = true;
      rec = cvRect(x, y, 0, 0);
    }
    break;

    // left button up
    case CV_EVENT_LBUTTONUP:{
      flg_drawing = false;
      flg_train = true;
      flg_pause = false;
      if (rec.width<0){
        rec.x += rec.width;
        rec.width *= -1;
      }
      if (rec.height<0){
        rec.y += rec.height;
        rec.height *= -1;
      }
    }
    break;

  }
};

// keyboard callback
void fun_keyboard_callback(clsParameters* prms, cv::Mat &img, char key){

  // keys actions
  switch (key){

    // ESC -> exit
    case 27:
      // message
      printf("\n!! EXIT\n\n");
      // exit program
      flg_exit = true;
      break;

    // space -> detection
    case 32:
      if (flg_detection){
        // switch off detection
        flg_detection = false;
      }
      else{
        // message
        printf("\n!! Detection\n\n");
        // enable detection
        flg_detection = true;
      }
      break;

    // p -> pause
    case 112:
      if (flg_pause){
      flg_pause = false;
    }
    else{
      printf("!! PAUSE\n");
      flg_pause = true;
      fun_draw_message(prms, img, "PAUSE", cvPoint(10, 30), cvScalar(255, 255, 0));
    }
  }
};







